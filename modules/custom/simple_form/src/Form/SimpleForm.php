<?php

namespace Drupal\simple_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class SimpleForm extends FormBase{
	/**
   * (@inherit)
   */
	public function getFormId(){
		return 'simple_form';
	}
	/**
   * (@inherit)
   */	
	public function buildForm(array $form, FormStateInterface $form_state) {
	
	 $form ['first_name'] = array(
		'#type' => 'textfield',
		'#title' => $this-> t('First Name'),
		'#size' => 30,
		'required' => TRUE,
	 );
	 $form ['last_name'] = array(
		'#type' => 'textfield',
		'#title' => $this-> t('Last Name'),
		'#size' => 30,
		'required' => TRUE,
	 );
	 $form['gender'] = array(
		'#type'=> 'radios',
		'#title'=> 'Gender',
		'#options'= array(
		'male'=> $this-> t('male'),
		'female'=>$this-> t('female'),
		),
	    'required'=> TRUE,
	 );
	 $form['submit']= array(
	 '#type'=> 'submit',
	 '#value'=> $this-> t('Submit Form'),
	 );
	 
	 return $form;
	}
	public function submitForm(array &$form, FormStateInterface $form_state)
	{
		drupal_set_message('Your First name: '.$form_state->getValue('first_name').
      ' Your Last name: '.$form_state->getValue('last_name').
      ' Gender: '.$form_state->getValue('gender'),'warrning');
	}
}

?>